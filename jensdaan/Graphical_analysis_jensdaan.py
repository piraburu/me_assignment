import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal
import statsmodels.graphics.tsaplots

data = pd.read_excel('../data/MVE_assignment_2020_dataset.xlsx')
data = data.rename(columns={"cntry.name": "Country", "year": "Year", "mean_pre":"Precipitation",   "mean_rad":"Radiation", "mean_tmp":"Temperature", "NY.GDP.MKTP.KD":"GDP", "NY.GDP.PCAP.KD":"GDP_Capita", "SP.POP.TOTL":"Population", "AG.LND.AGRI.K2":"Agricultural_land", "AG.PRD.CROP.XD":"Crop_production"})

#drop identification columns for country since we already have the name
data = data.drop(['ISO_N3', 'ISO_C3'], axis=1)

#assumption: remove string data which has missing values (see years 2017, 2018, 2019 - Norway)
data[data.columns[1:]] = data[data.columns[1:]].apply(lambda x: pd.to_numeric(x, errors='coerce'))
data = data.dropna()
#data = data.reset_index().drop(['index'], axis = 1)
data = data.set_index(['Country', 'Year'])

# declare dataframes

logdata = data.copy()
logdata = np.log(logdata)

diffdata = data.copy()
diffdata = diffdata.diff()

difflogdata = data.copy()
difflogdata = logdata.diff()

seconddiff_data = data.copy()
seconddiff_data = diffdata.diff()

detrended_data = data.copy()
detrended_data = detrended_data.apply(scipy.signal.detrend)

demeaned_data = data.copy()
demeaned_data = demeaned_data - demeaned_data.mean()

######### functions to plot data for 2 countries and all variables -> log/diff/difflog etc #######

test_data_norway = data.iloc[data.index.get_level_values('Country') == 'Norway']
test_data_thailand = data.iloc[data.index.get_level_values('Country') == 'Thailand']
  
fig, axs = plt.subplots(8,1, figsize=(10,20))
for ax, variable in zip(axs, test_data_norway.columns): 
    ax.set_title(f'{variable}')
    ax.plot(test_data_norway.index.get_level_values('Year'), test_data_norway[variable]) 
fig.tight_layout()

fig, axs = plt.subplots(8,1, figsize=(10,20))
for ax, variable in zip(axs, test_data_thailand.columns): 
    ax.set_title(f'{variable}')
    ax.plot(test_data_thailand.index.get_level_values('Year'), test_data_thailand[variable]) 
fig.tight_layout()





