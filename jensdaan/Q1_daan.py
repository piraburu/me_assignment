import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal
from statsmodels.graphics.tsaplots import plot_acf

# read data
df = pd.read_excel('../data/MVE_assignment_2020_dataset.xlsx')

# drop irrelevant columns and set index
df.drop(['ISO_N3', 'ISO_C3'], axis=1, inplace=True)
df = df.set_index(['year','cntry.name']).stack().unstack([1,2])

# clean string values 
df = df.apply(lambda x: pd.to_numeric(x, errors='coerce')).astype(float)

# define values for analysis
dfs = {
    'Levels'           : df.copy(),
    'Percentage Change': df.copy().pct_change(),
    'First Difference' : df.copy().diff(),
    'Second Difference': df.copy().diff().diff(),
    'Logarithmic'      : np.log(df.copy()),
    'Log Difference'   : np.log(df.copy()).diff(),
    'Detrended'        : df.copy().dropna().apply(scipy.signal.detrend), # drop alle rows nu met NA
    'Demeaned'         : df.copy() - df.copy().mean()
    }

### function for plots
def plot_variables(df, title='...'):

    variables = list(reversed(sorted(list(set(df.columns.get_level_values(1))))))
    countries = ['Norway', 'Thailand'] #list(set(df.columns.get_level_values(0)))

    fig, axs = plt.subplots(4,2, figsize=(10,12), sharex=True)
    fig.suptitle(title, fontsize=14)

    for ax, var in zip(axs.reshape(-1), variables): 
        ax.set_title(f'{var}')
        for c in countries:
            ax.plot(df.index, df[c, var])

    fig.tight_layout()
    fig.subplots_adjust(top=0.92)

    return fig

###
for name, data in dfs.items():
    plot_variables(df=data, title=name)

### function for autocorrelation function plots
def plot_acfs(df, title='...'):
    
    variables = list(reversed(sorted(list(set(df.columns.get_level_values(1))))))
    countries = ['Norway', 'Thailand']
    mcol = pd.MultiIndex.from_product([countries, variables])

    fig, axs = plt.subplots(4, 4, figsize=(20,12), sharex=True, sharey=True)
    fig.suptitle(title, fontsize=14)

    # country names
    plt.figtext(0.25, 0.94, countries[0], size=13)
    plt.figtext(0.74, 0.94, countries[1], size=13)

    for ax, mcol in zip(axs.flatten('F'), mcol): 
        for c in countries:
            plot_acf(df[mcol].dropna(), ax=ax, lags=20)
            ax.set_title(f'{mcol[1]}')
        
    fig.tight_layout()
    fig.subplots_adjust(top=0.90)

    return fig

for name, data in dfs.items():
    plot_acfs(df=data, title=name)




# test = dfs['Second Difference']['Norway', 'mean_tmp']

# p = )
# p.gca().lines[0].set_color("black")
# p.gca().lines[-1].set_color("black")

# p.gca().vlines[0].set_color('grey')

# dir(p.gca().vlines)

# fig, ax = plt.subplots()
# plot_acf(test.dropna(), ax=ax)

# clss = fig.findobj(lambda x: hasattr(x, 'get_color'))
# [c.set_color('black') for c in clss]

# fig.findobj(lambda x: x.get_color() == 'lightblue')

# ax.get_lines()[0].set_color('black')
# ax.get_lines()[1].set_color('black')



